/**
 * Display class for the nmm game
 * 
 * Contains a draw function which will output the board to console
 */
package com.peter.nmm.view;

import com.peter.nmm.model.Board;
import com.peter.nmm.model.Spot;

public class Display {

	/**
	 * Draw the required board to the console
	 * @param aBoard the board to draw
	 */
	public static void drawBoard(Board aBoard) {

		// draw coordinates
		System.out.println("  0 1 2 3 4 5 6");
		
		// iterate over the board, drawing each spot that is not a null spot
		for (int i = 0; i < Board.BOARDSIZE; i++) {
		
			System.out.print(i + "|");
			
			for (int j = 0; j < Board.BOARDSIZE; j++) {
				Spot currSpot = aBoard.getSpot(j, i);
				if (currSpot == null) {
					System.out.print(" ");
				} else {
					System.out.print(currSpot.asString());
				}
				System.out.print(" ");
			}
			System.out.print("\n");
		}
	}
}
