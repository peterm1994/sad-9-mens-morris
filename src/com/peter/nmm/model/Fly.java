/**
 * Fly move implementation
 * 
 * @author Peter Mandile
 */
package com.peter.nmm.model;

public class Fly implements IMove {

	// variables are stored so the move can be undone
	private Piece myPiece;
	private final Player myPlayer;
	private final Spot initSpot;
	private final Spot destSpot;
	
	/**
	 * Take the coordinates of the source and target spots to move a piece to
	 * @param currPlayer the current player
	 * @param initSpot initial source of the piece
	 * @param destSpot destination spot of the piece
	 */
	public Fly(Player currPlayer, Spot initSpot, Spot destSpot) {
		this.myPlayer = currPlayer;
		this.initSpot = initSpot;
		this.destSpot = destSpot;
	}

	/**
	 * Validate the move according to the game rules
	 */
	@Override
	public boolean validate() {
		// check destination spot is free and valid
		if (destSpot == null || !destSpot.isFree()) {
			return false;
		}
		// check the initial spot contains a piece and is valid
		if (initSpot == null || initSpot.isFree()) {
			return false;
		}
		// check that the chosen piece belongs to the player
		if (initSpot.getPiece().getPlayer() != this.myPlayer) {
			return false;
		}
		// if all checks pass, we can validate the move
		return true;
	}
	
	/**
	 * Apply the fly move action
	 */
	@Override
	public void apply() {
		this.myPiece = initSpot.getPiece();
		this.myPiece.setSpot(destSpot);
	}

	/**
	 * Undo the action. This undoes any affects the apply() function has
	 */
	@Override
	public void undo() {
		this.myPiece.setSpot(this.initSpot);
	}

	/**
	 * Getter for the piece that is being moved
	 */
	@Override
	public Piece getPiece() {
		return this.myPiece;
	}
	
	/**
	 * Debug string output
	 */
	@Override
	public String asString() {
		return "FLY MOVE: " + this.initSpot.coords() + " --> " + this.myPiece.getSpot().coords();
	}
}
