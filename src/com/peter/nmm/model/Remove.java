/**
 * Remove move implementation
 * 
 * @author Peter Mandile
 */
package com.peter.nmm.model;

public class Remove implements IMove {
	
	// variables are stored so the move can be undone
	private final Player myPlayer;
	private final Spot targetSpot;
	
	private Spot prevSpot;
	private Piece myPiece;
	
	/**
	 * The player performing the move and the spot where the target piece is
	 * @param player current player
	 * @param targetSpot target spot
	 */
	public Remove(Player player, Spot targetSpot) {
		this.myPlayer = player;
		this.targetSpot = targetSpot;
	}
	
	/**
	 * Validate the piece can be removed
	 * It must not be in a mill if possible
	 */
	@Override
	public boolean validate() {
		
		// check the spot is valid, contains a piece and the piece belongs to the target player, not the current player
		if (this.targetSpot == null || this.targetSpot.isFree() || this.targetSpot.getPiece().getPlayer() == this.myPlayer) {
			return false;
		}
		
		Player otherPlayer = this.targetSpot.getPiece().getPlayer();
		
		Board aBoard = Board.getInstance();
		if (otherPlayer.pieceCount() > 3 && aBoard.pieceInMill(this.targetSpot.getPiece())) {
			// if there are pieces available that aren't in mills, this is not allowed
			if (otherPlayer.hasPieceNotInMill()) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Apply the removal
	 */
	@Override
	public void apply() {
		
		// get the affected piece
		this.myPiece = this.targetSpot.getPiece();
		this.prevSpot = this.targetSpot;
		
		// set the spot to null, removing it from the board.
		// the piece still belongs to the player and is not destroyed
		this.myPiece.setSpot(null);
	}
	
	/**
	 * Undo actions performed by the apply() function
	 */
	@Override
	public void undo() {
		this.myPiece.setSpot(this.prevSpot);
	}

	/**
	 * Get removed piece
	 */
	@Override
	public Piece getPiece() {
		return this.myPiece;
	}

	/**
	 * Get move as a string
	 */
	@Override
	public String asString() {
		return "REMOVE MOVE: " + this.prevSpot.coords() + " --> " + "REMOVED";
	}
}
