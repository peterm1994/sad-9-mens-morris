/**
 * Human player implementation
 * 
 * Can create move commands as requested from the Game controller.
 * 
 * As a human class, external input is required to make a move
 * 
 * @author Peter Mandile
 */
package com.peter.nmm.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.peter.nmm.controller.*;

public class Human extends Player {
	
	// string match for undo command
	private static final String UNDO_COMMAND = "UNDO";
	
	/**
	 * Set the player id when instantiating
	 * @param id the player id
	 */
	public Human(int id) {
		
		this.playerID = id;
	}

	/**
	 * Create a Move object for the required move type
	 */
	@Override
	public IMove getMoveCommand(int moveType) {
		IMove myMove;
		
		// print instructions for the user
		System.out.print("Player " + this.playerID + ", ");
		
		switch (moveType) {
		case Game.MOVE_FLY:
			System.out.println("Enter fly move in the form <source> <destination>:");
			break;
		case Game.MOVE_PLACE:
			System.out.println("Enter coordinates to place piece:");
			break;
		case Game.MOVE_REMOVE:
			System.out.println("Enter coordinates of a piece to remove:");
			break;
		case Game.MOVE_SLIDE:
			System.out.println("Enter slide move in the form <source> <destination>:");
			break;
		}
		
		String playerInput = "";
		
		// attempt to get input from the console
		try {
			playerInput = getInput();
		} catch (IOException e) {
			System.out.println("Invalid input");	
		}

		// check for an undo command. 
		// first make uppercase for better matching
		if (playerInput.toUpperCase().equals(UNDO_COMMAND)) {
			return Game.MOVE_UNDO;
		}
		
		// player input array as integers.
		int[] pI;

		Spot initSpot = null;
		Spot destSpot = null;

		// attempt to extract the required information from the player input
		try {
			
			pI = charToIntArray(playerInput.toCharArray());
			
			Board gameBoard = Board.getInstance();
			// if we required 2 coordinates, the length of input will be 5 (xy xy)
			if (pI.length == 5) {
				initSpot = gameBoard.getSpot(pI[0], pI[1]);
				destSpot = gameBoard.getSpot(pI[3], pI[4]);
			// a single coordinate length is 2 (xy)
			} else if (pI.length == 2){
				destSpot = gameBoard.getSpot(pI[0], pI[1]);
			}
			// catch invalid coordinates (if the array index is not allowed)
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Invalid Input Coordinate");
		}

		// create the required move type, passing in required information
		switch (moveType) {
		case Game.MOVE_FLY:
			myMove = new Fly(this, initSpot, destSpot);
			break;
		case Game.MOVE_PLACE:
			myMove = new Place(this, destSpot);
			break;
		case Game.MOVE_REMOVE:
			myMove = new Remove(this, destSpot);
			break;
		case Game.MOVE_SLIDE:
			myMove = new Slide(this, initSpot, destSpot);
			break;
        default:
            // this should never be reached if called using a defined move type
            throw new UnknownError("The requested move type does not exist");
		}
		
		// check the move is valid
		if (myMove.validate()) {
			// apply the move, returning it to the Game class
			myMove.apply();
			return myMove;
		} else {
			// the move was invalid, so prompt the player again until a valid move is created
			System.out.println("Invalid Move.");
			return getMoveCommand(moveType);
		}
	}
	
	/**
	 * Get input from the console as a string
	 * @return string input by the user
	 * @throws IOException
	 */
	private String getInput() throws IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        return br.readLine();

	}
	
	/**
	 * Convert a string array from the user input into integers,
	 * so they can be used as coordinates in the board array
	 * @param charArray the character array
	 * @return an equivalent integer array
	 */
	private int[] charToIntArray(char[] charArray) {
		int[] intArray = new int[charArray.length];
		
		for (int i = 0; i < charArray.length; i++){
	        intArray[i] = charArray[i] - '0';
	    }
		
		return intArray;
	}
}
