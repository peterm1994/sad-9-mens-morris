/**
 * Piece object implementation
 * 
 * Pieces have a 2 way reference to the spot they are on
 * They also know which player they belong to
 * 
 * @author Peter Mandile
 */
package com.peter.nmm.model;

import java.util.Set;

public class Piece {

	// the player that owns the piece
	private final Player player;

	// the spot the piece is occupying
	private Spot mySpot;
	
	/**
	 * Constructor allows the player to be set,
	 * and adds the piece to that player's collection
	 * @param player the owner of the piece
	 */
	public Piece(Player player) {
		this.player = player;
		player.addToPieces(this);
	}
	
	/**
	 * Player getter
	 * @return the player
	 */
	public Player getPlayer() {
		return this.player;
	}
	
	/**
	 * Spot getter
	 * @return the spot
	 */
	public Spot getSpot() {
		return this.mySpot;
	}
	
	/**
	 * Set the spot that the piece occupies.
	 * The piece can be placed on a null spot, signifying it is not on the board
	 * @param aSpot the spot to put the piece on
	 */
	public void setSpot(Spot aSpot) {
		// remove old reference spot
		if (this.mySpot != null) {
			this.mySpot.removePiece();
		}
		this.mySpot = aSpot;
		// if the spot exists, create the backreference (in the spot)
		if (aSpot != null) {
			aSpot.setPiece(this);
		}
	}
	
	/**
	 * Return the player Id of the piece for drawing
	 */
	public String toString() {
		return String.valueOf(player.playerID);
	}

	/**
	 * Check a valid slid move can be performed
	 * @return true if a valid slide move exists
	 */
	public boolean hasValidMove() {
		
		// get the neighbors of this spot
		Set<Spot> neighbours = this.mySpot.getNeighbours();
		
		// check that an adjacent spot is free
		for (Spot aNeighbour : neighbours) {
			if (aNeighbour.isFree()) {
				return true;
			}
		}
		
		return false;
	}

    /**
     * Check if the piece is on the game board without exposing the spot
     * @return true if on game board
     */
    public boolean isOnBoard() {
        return this.mySpot != null;
    }
}
