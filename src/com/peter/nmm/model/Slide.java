/**
 * Slide move implementation
 * 
 * @author Peter Mandile
 */
package com.peter.nmm.model;

public class Slide implements IMove {

	// variables are stored so the move can be undone
	private final Player myPlayer;
	private final Spot initSpot;
	private final Spot destSpot;

    private Piece myPiece;

	/**
	 * Take the coordinates of the source and target spots to move a piece to
	 * @param currPlayer the current player
	 * @param initSpot initial source of the piece
	 * @param destSpot destination spot of the piece
	 */
	public Slide(Player currPlayer, Spot initSpot, Spot destSpot) {
		this.myPlayer = currPlayer;
		this.initSpot = initSpot;
		this.destSpot = destSpot;
	}
	
	/**
	 * Check that the move is valid
	 */
	@Override
	public boolean validate() {
		
		// check the destination is valid and empty
		if (destSpot == null || !destSpot.isFree()) {
			return false;
		}
		// check the initial piece exists
		if (initSpot == null || initSpot.isFree()) {
			return false;
		}
		// check the piece belongs to the current player
		if (initSpot.getPiece().getPlayer() != this.myPlayer) {
			return false;
		}
		// check the target spot is a neighbour of the source
		if (!initSpot.isNeighbour(destSpot)) {
			return false;
		}

		return true;
		
	}

	/**
	 * Apply the move
	 */
	public void apply() {
		this.myPiece = initSpot.getPiece();
		
		this.myPiece.setSpot(destSpot);
	}
	
	/**
	 * Undo the apply() move
	 */
	@Override
	public void undo() {
		this.myPiece.setSpot(this.initSpot);
	}

	/**
	 * Get the moved piece
	 */
	@Override
	public Piece getPiece() {
		return this.myPiece;
	}
	
	/**
	 * The move as a string
	 */
	@Override
	public String asString() {
		return "SLIDE MOVE: " + this.initSpot.coords() + " --> " + this.myPiece.getSpot().coords();
	}
}