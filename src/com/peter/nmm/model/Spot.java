/**
 * The Spot object implementation.
 * A spot has coordinates on the game board, and knows the neighbours of it
 * 
 * A spot can also contain a piece\
 * 
 * @author Peter Mandile
 */
package com.peter.nmm.model;

import java.util.HashSet;
import java.util.Set;

public class Spot {

	// position on the gameboard
	private final int xpos;
	private final int ypos;
	
	// all neighbours of the spot
	// NOTE: this is only calculated when it is required
	private Set<Spot> neighbours = null;
	
	// piece on the spot
	private Piece myPiece = null;
	
	/**
	 * Create a spot for the desired position
	 * @param x x board coordinate
	 * @param y y board coordinate
	 */
	public Spot(int x, int y) {
		this.xpos = x;
		this.ypos = y;
	}
	
	/**
	 * Get the piece on this spot
	 * @return the piece on this spot
	 */
	public Piece getPiece() {
		return this.myPiece;
	}
	
	/**
	 * Check if a piece is on this spot
	 * @return true if no piece is in this spot
	 */
	public boolean isFree() {
		return (this.myPiece == null);
	}

	/**
	 * Calculate neighbouring spots
	 * This information is saved for future use
	 */
	private void calculateNeighbors() {
		
		this.neighbours = new HashSet<Spot>();
		
		int curPos;
		
		// move in each direction (up, down, left, right) and add the first found valid spot to
		// the neighbours set. If the array boundary is hit, no neighbour exists in that direction.
		// we must also check if the current position is the centre of the board, as no neighbours can be created here
		
		// left
		curPos = xpos;		
		while (curPos >= 0 && !(curPos == 3 && ypos == 3)) {
			if (Board.validspots[curPos][ypos] == 1 && curPos != this.xpos) {
				this.neighbours.add(Board.getInstance().getSpot(curPos, this.ypos));
				break;
			}
			curPos--;
		}
		
		// right
		curPos = xpos;		
		while (curPos < Board.BOARDSIZE && !(curPos == 3 && ypos == 3)) {
			if (Board.validspots[curPos][ypos] == 1 && curPos != this.xpos) {
				this.neighbours.add(Board.getInstance().getSpot(curPos, this.ypos));
				break;
			}
			curPos++;
		}
		
		// up
		curPos = ypos;		
		while (curPos >= 0 && !(curPos == 3 && xpos == 3)) {
			if (Board.validspots[xpos][curPos] == 1 && curPos != this.ypos) {
				this.neighbours.add(Board.getInstance().getSpot(this.xpos, curPos));
				break;
			}
			curPos--;
		}
		
		// down
		curPos = ypos;		
		while (curPos < Board.BOARDSIZE && !(curPos == 3 && xpos == 3)) {
			
			if (Board.validspots[xpos][curPos] == 1 && curPos != this.ypos) {
				this.neighbours.add(Board.getInstance().getSpot(this.xpos, curPos));
				break;
			}
			curPos++;
		}
	}
	
	/**
	 * Check if another spot is a neighbour of this
	 * @param other the other spot
	 * @return true if a neighbour
	 */
	public boolean isNeighbour(Spot other) {
		// check the other spot exists
		if (other == null) {
			return false;
		}
		// calculate the neighbours if it has not yet been done
		if (this.neighbours == null) {
			calculateNeighbors();
		}
		return (this.neighbours.contains(other));	
	}
	
	/**
	 * Get all neighbouring spots
	 * @return all neighbours
	 */
	public Set<Spot> getNeighbours() {
		// calculate if required
		if (this.neighbours == null) {
			calculateNeighbors();
		}
		return this.neighbours;
	}
	
	/**
	 * Place a piece on this spot
	 * @param aPiece the piece to set
	 */
	public void setPiece(Piece aPiece) {
		this.myPiece = aPiece;
	}
	
	/**
	 * Remove the piece from this spot
	 */
	public void removePiece() {
		this.myPiece = null;
	}
	
	/**
	 * Return the piece as a string if there is one,
	 * else just a special symbol
	 * @return string representation of the spot
	 */
	public String asString() {
		
		if (myPiece != null) {
			return myPiece.toString();
		}
		return "+";
	}
	
	public String toString() {
		return coords();
	}
	
	public String coords() {
		return "(" + xpos + "," + ypos + ")";
	}
}
