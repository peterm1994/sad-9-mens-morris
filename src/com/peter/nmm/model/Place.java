/**
 * Place move implementation
 * 
 * @author Peter Mandile
 */
package com.peter.nmm.model;

import com.peter.nmm.controller.Game;

public class Place implements IMove {

	// variables are stored so the move can be undone
	private final Spot destSpot;
	private final Player myPlayer;
	private Piece myPiece;
	
	/**
	 * Take the coordinates of the target spot to place the piece
	 * @param currPlayer the current player
	 * @param destSpot destination spot of the piece
	 */
	public Place(Player currPlayer, Spot destSpot) {
		this.destSpot = destSpot;
		this.myPlayer = currPlayer;
	}

	/**
	 * Validate the move according to the game rules
	 */
	public boolean validate() {
		// check the destination spot is valid and free
        return !(destSpot == null || !destSpot.isFree());
    }
	
	/**
	 * Apply the move
	 */
	public void apply() {

		// tell the game that a piece has been placed
		Game.piecesToPlace--;
		
		// Create the piece, and place where required
		this.myPiece = new Piece(this.myPlayer);
		this.myPiece.setSpot(destSpot);
	}

	/**
	 * Undo this move
	 */
	@Override
	public void undo() {
		
		Game.piecesToPlace++;
		this.myPiece.setSpot(null);
	}

	/**
	 * String representation of the move
	 */
	@Override
	public String asString() {
		return "PLACE MOVE: " + "NEW PIECE" + " --> " + this.myPiece.getSpot().coords();
	}

	/**
	 * Getter for affected piece
	 * @return the piece
	 */
	@Override
	public Piece getPiece() {
		return this.myPiece;
	}
}
