/**
 * Move command interface
 * 
 * Each move can be validated, applied and undone.
 * 
 * @author Peter Mandile
 */
package com.peter.nmm.model;

public interface IMove {
	
	/**
	 * Apply this move. validate() should be run first to check move is valid
	 */
	public void apply();
	
	/**
	 * Undo any actions that the apply() move has done
	 */
	public void undo();
	
	/**
	 * Validate the move according to the specific move rules
	 * @return true on valid
	 */
	public boolean validate();
	
	/**
	 * Get the piece affected by this move
	 * @return the affected piece
	 */
	public Piece getPiece();
	
	/**
	 * Debug command as a string
	 * @return string representation of the move being performed
	 */
	public String asString();
	
}
