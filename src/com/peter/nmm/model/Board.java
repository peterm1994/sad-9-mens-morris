/**
 * Board class for the nine men's morris game.
 * 
 * This class is responsible for tracking all spots on a board, as well as
 * calculating if mills are formed
 * 
 * @author Peter Mandile
 */
package com.peter.nmm.model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Board {

	// singleton implementation
    private static final Board INSTANCE = new Board();
    
    public static Board getInstance() {
        return INSTANCE;
    }
		
    // board array size
	public static final int BOARDSIZE = 7;
	
	// invalid spots are null objects
	private static final Spot INVALID_SPOT = null;
	
	// all valid spots are defined in an int array
	public static int[][] validspots;
	
	// keep all Spot objects in an array, with coordinate corresponding to board positions.
	private final Spot[][] spots;
	
	// mills are stored as sets of 3 spots
	private final Set<Set<Spot>> mills;
	
	/**
	 * The board constructor defines
	 */
	private Board() {
		
		validspots = new int[][] {  {1, 0, 0, 1, 0, 0, 1},
								    {0, 1, 0, 1, 0, 1, 0},
								    {0, 0, 1, 1, 1, 0, 0},
								    {1, 1, 1, 0, 1, 1, 1},
								    {0, 0, 1, 1, 1, 0, 0},
								    {0, 1, 0, 1, 0, 1, 0},
								    {1, 0, 0, 1, 0, 0, 1}};
		
		this.spots = new Spot[7][7];
		// use the array of valid spots to determine whether a spot is created or not at each position
		for (int i = 0; i < BOARDSIZE; i++) {
			for (int j = 0; j < BOARDSIZE; j++) {
				if (validspots[i][j] == 1) {
					this.spots[i][j] = new Spot(i, j);
				} else {
					this.spots[i][j] = INVALID_SPOT;
				}
			}
		}		
		
		
		// set up the mills
		mills = new HashSet<Set<Spot>>();
		Set<Spot> currxMill = new HashSet<Spot>();
		Set<Spot> curryMill = new HashSet<Spot>();
		
		// iterate over each board position
		// mills are created by adding 3 sequential Spots together.
		// this is done both vertically and horizontally in the same loop
		for (int i = 0; i < BOARDSIZE; i++) {
			for (int j = 0; j < BOARDSIZE; j++) {
				if (validspots[i][j] == 1) {
					currxMill.add(this.spots[i][j]);
					if (currxMill.size() == 3) {
						mills.add(currxMill);
						currxMill = new HashSet<Spot>();
					}
				}
				if (validspots[j][i] == 1) {
					curryMill.add(this.spots[j][i]);
					if (curryMill.size() == 3) {
						mills.add(curryMill);
						curryMill = new HashSet<Spot>();
					}
				}
			}
		}
	}
	
	/**
	 * Get a spot in a requested position on the game board
	 * @param i x coordinate
	 * @param j y coordinate
	 * @return the spot at the location. can be null if no spot exists there
	 */
	public Spot getSpot(int i, int j) {
		return this.spots[i][j];
	}
	
	/**
	 * Check if a piece is part of a mill
	 * @param aPiece piece to check
	 * @return true if in mill, else false
	 */
	public boolean pieceInMill(Piece aPiece) {

		Spot aSpot = aPiece.getSpot();
		Player currPlayer = aPiece.getPlayer();
		
		// iterate over all mills, checking for the spot
		for (Set<Spot> aMill : this.mills) {
			if (aMill.contains(aSpot)) {
				// check all spots contain the same colour
				boolean validMill = true;
				Iterator<Spot> spotIter = aMill.iterator();
				while (validMill && spotIter.hasNext()) {
					Spot currSpot = spotIter.next();
					if (!currSpot.isFree()) {
						if (currSpot.getPiece().getPlayer() != currPlayer) {
							validMill = false;
						}
					} else {
						validMill = false;
					}
				}
				if (validMill) {
					return true;
				}
			}
		}
		return false;
	}	
}
