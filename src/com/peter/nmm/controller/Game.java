/**
 * Controller class for the nine men's morris game.
 * A game can be set up, and the main game loop is run.
 * 
 * This class also tracks all moves made, and can undo them as desired.
 * 
 * @author Peter Mandile
 */
package com.peter.nmm.controller;

import java.util.Stack;

import com.peter.nmm.model.*;
import com.peter.nmm.view.Display;

public class Game {

	// Ensure this class can only be instantiated once, and has an access point 
    private static final Game INSTANCE = new Game();
    

    // access this instance
    public static Game getInstance() {
        return INSTANCE;
    }
	
    // keep track of how many pieces must be placed in the placing phase
	public static int piecesToPlace = 18;
	
	// the two players
	private Player p1;
	private Player p2;
	
	// track the current player here
	private Player currentPlayer;
	
	// a reference to the board object.
	private final Board gameBoard;
	
	// all moves will be pushed onto this stack, and can be popped off to undo them
	private Stack<IMove> moveStack;
	
	// flag for move types - will be true if a piece must be removed next turn
	private boolean removeRequired = false;

	// a flag to check if any valid moves are left for the next player
	private boolean noValidMoves = false;
	
	// define the move types
	public static final int MOVE_PLACE = 0;
	public static final int MOVE_FLY = 1;
	public static final int MOVE_SLIDE = 2;
	public static final int MOVE_REMOVE = 3;
	
	public static final IMove MOVE_UNDO = null;
	
	
    // use a private constructor to ensure another class cannot instantiate this one
    private Game() {
    	// create a reference to the board for easier access
		this.gameBoard = Board.getInstance();
		this.moveStack = new Stack<IMove>();
    }

	/**
	 * The game is started using this function, passing in the players
	 * The first player specified will go first
	 * @param p1 first player
	 * @param p2 second player
	 */
	public void startGame(Player p1, Player p2) {
		
		// set the 
		this.p1 = p1;
		this.p2 = p2;

		this.currentPlayer = this.p1;
		
		// main game loop
		// while the game is not over, or a player still has valid moves, loop
		while (!gameOver() && !noValidMoves) {
			// pass the board to the display to draw it.
			Display.drawBoard(gameBoard);
			turn();
		}
		
		// switch the played for the winner
		setNextPlayer();
		System.out.print("Player " + currentPlayer + " wins!");
		
	}
	
	/**
	 * The main turn function. Analyses the state of the game, and requests a move from
	 * the current player
	 */
	private void turn() {
		
		IMove nextMove;
		
		// if the previous turn flagged for a piece removal
		if (removeRequired) {
			IMove removeMove = this.currentPlayer.getMoveCommand(MOVE_REMOVE);
			
			// check for an undo move
			if (removeMove == MOVE_UNDO) {
				undoLast();
				return;
			} else {
				// push current move onto move stack, cycle player
				System.out.println(removeMove.asString());
				this.moveStack.push(removeMove);
				setNextPlayer();
			}
			// reset flag
			removeRequired = false;
			
		} else {
			// check for the placing phase
			if (piecesToPlace > 0) {
				// request a place move
				nextMove = this.currentPlayer.getMoveCommand(MOVE_PLACE);
			} else {
				// we can fly if we have 3 pieces left
				if (this.currentPlayer.pieceCount() == 3) {
					nextMove = this.currentPlayer.getMoveCommand(MOVE_FLY);
				} else {
					// check for valid moves
					if (!this.currentPlayer.hasValidMove()) {
						this.noValidMoves = true;
						return;
					}
					nextMove = this.currentPlayer.getMoveCommand(MOVE_SLIDE);
				}
			}
			// check for undo request, else perform normal move
			if (nextMove == MOVE_UNDO) {
				undoLast();
				return;
			} else {
				System.out.println(nextMove.asString());
				this.moveStack.push(nextMove);
			}
			
			// if we have formed a mill, a removal is required.
			// we also do not toggle the current player
			if (this.gameBoard.pieceInMill(nextMove.getPiece())) {
				removeRequired = true;
			} else {
				removeRequired = false;
				setNextPlayer();
			}
		}
	}

	/**
	 * Checks the piece count of the player to determine if the game is over
	 * @return true if win condition is met
	 */
	private boolean gameOver() {
		return (piecesToPlace == 0 && currentPlayer.pieceCount() == 2);
	}
	
	/**
	 * Simply toggles the current player
	 */
	private void setNextPlayer() {
		if (this.currentPlayer.equals(this.p1)) {
			this.currentPlayer = this.p2;
		} else {
			this.currentPlayer = this.p1;
		}
	}
	
	/**
	 * Undo the last move.
	 * Moves are kept in a stack, so can be undone all the way back
	 * to the start of the game
	 */
	private void undoLast() {
		if (!this.moveStack.isEmpty()) {
			IMove undoMove = this.moveStack.pop();
			undoMove.undo();
			// if we undo a remove, we do not swap current player
			if (undoMove.getClass() != Remove.class) {
				setNextPlayer();
			}
		} else {
			System.out.println("There are no further moves to undo.");
		}
	}
}
