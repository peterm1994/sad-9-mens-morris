package com.peter.nmm;

import com.peter.nmm.controller.Game;
import com.peter.nmm.model.*;

public class Main {

	public static void main(String[] args) {
		
		System.out.println("Nine Men's Morris");
		System.out.println("Created by Peter Mandile");
		System.out.println("");
		
		Player p1 = new Human(1);
		Player p2 = new Human(2);
		
		Game aGame = Game.getInstance();

		aGame.startGame(p1, p2);
	}
}